# -WM_CRM 小程序接口文档

前言：

1. 请求方式post
2. 除登录小程序以外 其他请求都需要 在header中 传 "token"

## 1首页

### 1.1 登录

#### 1.1.1小程序登录

请求地址 ：/miniApp/userInfo/login
请求参数 ：

```
{
"code":{小程序登录前台返回的code}
}
```

响应参数：

```
{
code:0,
data:{
    "token":{登录crm小程序后台的token 之后的请求都要带这个参数 }
    "userInfo":{
        "dataCreated": "2021-07-03 11:12:57"
        "grapeUserId": {ERP用户ID}
        "headFlag": {是否是店长}
        "headSculptureUrl": null
        "id": {userId}
        "imgId": {头像图片Id},
        "lastUpdated": "2021-07-03 11:13:09"
        "mobile":{手机号}
        "qrImg": {企业微信二维码}
        "qyUserId": {企业微信用户ID}
        "sessionKey": "DHOweOD15uySuIzpC8sTGg=="
        "shopId": {门店ID}
        "shopName": {门店名称}
        "userName": {用户名}
        "wxOpenId": {openId}
        }
	}
}

```

#### 1.1.2企业微信登录

请求地址：/miniApp/userInfo/qyLogin

请求参数：

```
{
"code":{企业微信登录前台返回的code}
}
```

响应参数：

```
{
code:0,
data:{
	"sessionKey": {sessionKey}
	"userId": {企业微信用户Id}
	}
}
```

#### 1.1.3 获取图片

请求地址：/miniApp/fileInfo/getImg

请求参数：

```
{
"id":{图片ID}//例如 userInfo 中的imgId
}
```

响应参数：

```
{
code:0,
data:{图片的二进制数据}
}
```

#### 1.1.4 保存会员信息

请求地址：/miniApp/userInfo/update

请求参数：

```
{
	 id:{小程序用户ID},
     userName:{用户名},
     wxOpenId:{openId},
     mobile:{手机号},
     headSculptureUrl:{微信获取头像后返回的url},
     encryptedData;{获取手机号时 的加密数据},
     iv;｛加密iv｝,
     sessionKey:{sessionKey},
     qwUserId：{企业微信Id},
     shopId:{门店Id},
     headFlag:{店长表示},
     qrImg:{企业微信二维码url}
}
```

响应参数：

```
{
	code:0,
	data:null,
	message:"操作成功"
}
```



#### 1.1.5 获取手机号

请求地址：/miniApp/userInfo/getMobile

请求参数：

```
{
    'encryptedData':{加密数据},
    'iv': {加密iv},
    'sessionKey': {微信登录的sessionKey},
    'userId': {当前登录userId}
}
```



响应参数：

```
{
    code:0,
    data:{手机号}
}
```

#### 1.1.6根据收据号查询ERP user

请求地址：/miniApp/userInfo/queryGrapeUserByMobile

请求参数：

```
{
	"mobile":{手机号}
}
```



响应参数：

```
{
    code:0,
    data:{
		mobile: {手机号}
        sex: {性别}
        shopId: {门店Id}
        shopName: {门店名称}
        userId: {grapeUserId}
        userName: {grapeUserName}
    }
}
```

### 1.2 会员

#### 1.2.1 会员数查询

请求地址：/miniApp/member/queryMemberCount

请求参数:

```
{
	"shopId":{门店ID}，
	"ifNew":{本店新增标识}
}
```



响应参数：

```
{
    code:0,
    data:{会员数}
}
```



#### 1.2.2 本月完善会员数查询

请求参数：/miniApp/queryMemberNewUpdateCount

请求参数：

```
{
	"shopId":{门店ID}
}
```



响应参数：

```
{
	code：0,
	data：{新完善会员数}
}
```

#### 1.2.3 会员分页查询

请求地址：/miniApp/member/queryMember

请求参数：

```
{
    "shopId": {门店ID},
    "customerServiceId": {grapeUserId},
    page: {
        currentNum: page,//从1开始
        pageSize: 10 //每次查询数
    },
    memberName: {手机号/姓名/会员卡号}
}
```



响应参数：

```
{
    code: 0
    data:{
        content: [
            {
                birthDate: {出生日期},
                bloodType: {血型/目前都是空不展示},
                curPoint: {当前积分},
                customerServiceName: {专属客服},
                dataCreated: "",
                issuanceDate: {开卡日期},
                lastUpdated: "",
                memberId: {会员Id},
                memberName: {会员名},
                memberNo: {会员卡号},
                memberTypeName: {会员类型},
                mobile: {手机号},
                nationName: {民族/目前多数为空不展示},
                sex: {性别},
                shopName: {开卡门店},
                sumConsumeAmount: {总消费},
                useStatus: {启用状态}
            }
        ]
        empty: false
        first: true
        last: false
        number: 0
        numberOfElements: 10
        pageable: {sort: {…}, offset: 0, pageNumber: 0, pageSize: 10, paged: true, …}
        size: 10
        sort: {sorted: false, unsorted: true, empty: true}
        totalElements: 11026
        totalPages: 1103
    }
    message: "操作成功"
}
```

#### 1.2.4 会员详情查询

请求地址：/miniApp/member/showMemberDtl

请求参数：

```
{
	"memberId":{会员ID}
}
```



响应参数：

```
{
	code: 0
    data:{
        address:{地址},
        age: {年龄},
        alwaysShopName: {长顾门店(近三个月)},
        amount: {余额},
        balance: {},
        count: {消费次数（近三个月）},
        curPoint: {当前积分},
        memberName: {姓名},
        memberNo: {卡号},
        memberTypeName: {会员类型},
        mobile: {电话},
        price: {客单价(近三个月)},
        sex: {性别},
        shopName: {开卡门店},
        shopShortName: {开卡门店简称}
    }
  
}
```

#### 1.2.5 会员消费记录

请求地址：/miniApp/member/showRtlFlow

请求参数：

```
{
		memberId: {会员Id},
        page: {
            currentNum: {当前页（从1开始）},
            pageSize: {每页显示条数}
        },
}
```



响应参数：

```
{
    code: 0
    data:{
        content: [
    		{
                amount: {金额},
                dataCreated: {},
                goodsId: {商品ID},
                goodsName:{商品名称},
                goodsQty: {数量},
                goodsSpecs: {规格},
                packName:{包装},
                shopName:{门店名称},
    		}
        ]
    }
  
}
```

#### 1.2.6 会员标签查询

请求地址：/miniApp/member/queryMemberLabel

请求参数：

```
{
	"memberId":{会员ID}
}
```



响应参数：

```
{
	code: 0
    data: [
        {
            labelColor: {标签颜色},
            labelName: {标签名称},
            relationId: {会员与标签关系Id}
        }
    ]
   
}
```

#### 1.2.7 删除会员标签

请求地址：/miniApp/member/deleteMemberLabel

请求参数：

```
{
	ids: [relationId]
}
```



响应参数：

```
{
	code:0,
	data:null,
	message:"操作成功"
}
```

#### 1.2.8  会员贴标签

请求地址：/miniApp/member/setLabel

请求参数：

```
{
	memberId: {会员ID},
    labelId: {标签ID},
    adminId:{当前用户Id 登录时返回的userInfo.id}
}
```

响应参数：

```
{
	code:0,
	data:null,
	message:"操作成功"
}
```

#### 1.2.9  添加标签时 标签查询

请求地址：/miniApp/label/queryLabel

请求参数：

```
{
   "goodsFlag": 0,//固定传0（查询非商品标签）
    page: {
        currentNum: 1,
        pageSize: 100000000 //（由于这里不需要分页 所以每页显示全部）当然如果是分页就正常传
    },
    "labelName":{名称/助记码}
}
```



响应参数：

```
{
code: 0
data:{
    content: [{
        attributeList: [{
        				attributeId: {属性ID},
                        attributeMCode: {属性助记码},
                        attributeName: {属性名称},
                        dataCreated:{},
                        inputManId:{},
                        lastUpdated:{},
                        }],
        dataCreated:{},
        describe: {描述},
        goodsFlag: {商品标识},
        labelColor: {标签颜色},
        labelId: {标签Id},
        labelMCode: {助记码},
        labelName: {标签名称},
        lastUpdated: {},
        taboo: {禁忌},
    }]
}

}
```

### 1.3 标签

#### 1.3.1 标签查询

**标签没有详情接口 查询时都查出来了 如需要详情页面 自己从查询结果集取**

请求地址：/miniApp/label/queryLabel

请求参数：

```
{
    page: {
        currentNum: {当前页},
        pageSize: {每页显示条数} 
    },
    "labelName":{名称/助记码}
}
```



响应参数：

```
{
code: 0
data:{
    content: [{
        attributeList: [{
        				attributeId: {属性ID},
                        attributeMCode: {属性助记码},
                        attributeName: {属性名称},
                        dataCreated:{},
                        inputManId:{},
                        lastUpdated:{},
                        }],
        dataCreated:{},
        describe: {描述},
        goodsFlag: {商品标识},0会员标签 1商品标签
        labelColor: {标签颜色},
        labelId: {标签Id},
        labelMCode: {助记码},
        labelName: {标签名称},
        lastUpdated: {},
        taboo: {禁忌},
    }]
}

}
```

### 

