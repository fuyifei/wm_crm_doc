# WM_CRM 接口文档

## 接口

sg 项目所有的接口请求方式都是 POST 形式，其中 content-type 必须是 application/json，接口编码必须为 application/json;charset=utf-8。

在接口请求过程中，需要在 header 中指定一个特殊的头，用来表明身份

```json
{
	"token":""
}
```

标准响应：

```json
{
    "code": 状态码,
    "message": 响应描述信息,
    "data":正常响应时返回的数据存放字段
}
```

### 一、管理员接口

#### 1.1 管理员登录接口

请求地址：/web/adminInfo/login

请求参数：

```json
{
    "loginName": {登录名称},
    "passwordHash": {登录密码}
}
```

响应参数：

```json
{
    "code":0,
    "message":"操作成功",
    "data":{
        "adminInfo":{
            "adminId":{人员ID},
            "loginName":{登录名},
            "fullName":{显示姓名},
            "mobile":{手机},
            "useStatus":{状态},//false：正常; true:删除
            "dataCreated":{创建日期},
            "lastUpdated":{修改日期}
        			},
   		"token":{认证字符串}
    		}

}
```

#### 1.2 企业微信用户绑定

请求地址：

请求参数：

响应参数：

1.3 企业微信部门查询

请求地址：/web/adminInfo/queryQwDept

请求参数：

```
{
无
}
```

响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "name": {部门名称},
            "id": {部门ID},
            "parentId": {上级部门ID}
        }
        ]
 }
```



1.4 企业微信人员查询

请求地址：/web/adminInfo/queryQwUser

请求参数：

```
{
"id":{部门ID（必填）}
}
```



响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "name": {人员名称},
            "userId": {人员ID}
        }
    ]
}
```



### 二、标签接口

#### 2.1 标签管理

##### 2.1.1 分页查询

请求地址：/web/label/queryPage

请求参数：

```json
{
    "page":{
    "pageSize": {每页显示数量(必填)},
    "currentNum":{当前页(必填)}
                 },

    "labelName":{ID/名称/助记码},
  	"labelAttributeDTOList":[
        {
           "attributeId":{标签属性ID}
        }
    ],
     "goodsFlag":{商品标识 0: 非商品标签；1：商品标签}
}
```

响应参数：

```json
{
    "code":0,
    "message":"操作成功",
    "data":{
        "content":[
            {
                "lastUpdated":{修改日期},
                "labelId":{标签ID},
                "taboo":{禁忌},
                "goodsFlag":{商品标识},//0：非商品标签 1：商品标签
                "attributeList":[ 
                    {"attributeId":{属性ID},
                     "attributeName":{属性名称}, 
                     "dataCreated":{属性创建时间},
                     "lastUpdated":{属性修改时间}}
                ],
                "describe":{测试描述},
                "dataCreated":{创建时间},
                "labelName":{标签名称},
                "labelMCode":{标签助记码},
                "labelColor":{标签颜色} //"#000000"
            }
        ]
    }
}
```

##### 2.1.2 新增

请求地址：/web/label/save

请求参数：

```json
{
    "labelName": {名称},
    "labelMCode":{助记码},
    "describe":{描述},
    "taboo":{禁忌},
    "goodsFlag":{商品标识},//0：非商品 1：商品
    "labelColor":{标签颜色},//"#FFFFFF"
    "labelAttributeDTOList":[{
                  attributeId:{标签属性Id}//见 标签属性 2.4.4 查询  
                 }]              
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.1.3 修改

请求地址：/web/label/save

请求参数：

```json
{
    "labelId":{ID(必填)},
    "labelName": {名称},
    "labelMCode":{助记码},
    "describe":{描述},
    "taboo":{禁忌},
    "labelColor":{标签颜色},//"#FFFFFF"
    "labelAttributeDTOList":[{
                  attributeId:{标签属性Id}//见 标签属性 2.4.4 查询  
                 }]   
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.1.4 删除(批量)

请求地址：/web/label/delete

请求参数：

```json
{
    "ids":[ID]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.1.5 查询

请求地址：/web/label/queryAll

请求参数：

```json
{
    "labelName":{ID/名称/助记码},
  	"labelAttributeDTOList":[
        {
           "attributeId":{标签属性ID}//见 标签属性 2.4.4 查询  
        }
    ],
    "goodsFlag":{商品标识},//0: 非商品标签；1：商品标签
	"goodsId":{要贴标签商品的id},
    "memberId"：{要贴标签会员的Id}
}
```

响应参数：

```json
{
    "code":0,
    "message":"操作成功",
    "data":[
            {
                "lastUpdated":{修改日期},
                "labelId":{标签ID},
                "taboo":{禁忌},
                 "goodsFlag":{商品标识}, // 0：非商品标签 1：商品标签
                "attributeList":[ 
                    {"attributeId":{标签属性ID},
                     "attributeName":{标签属性名称}, 
                     "dataCreated":{创建时间},
                     "lastUpdated":｛修改时间｝}
                ],
                "describe":{描述},
                "dataCreated":{创建时间},
                "labelName":{标签名称},
                "labelMCode":{标签助记码},
                "labelColor":{标签颜色}}
        ]
}
```

#### （暂停）2.2 荐药标签

##### 2.2.1 查询（标签）

请求地址：/web/label/queryPage

请求参数：

```json
{
    "page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                     			},
    "labelId":{标签标识},
    "labelName":{标签名称},
    "labelMCode":｛助记码},
    "recommendFlag":{荐药标识(填1)}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":｛｝
}
```

##### 2.2.2 根据标签查询组合

请求地址：/web/recommendDrugsCombo/queryPage

请求参数：

```json
{
    "pageSize": {每页显示数量(必填)},
    "currentNum":{当前页(必填)},
    "labelId":{标签标识}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":｛｝
}
```



##### 2.2.3 新增推荐组合

请求地址：/web/recommendDrugsCombo/saveDoc

请求参数：

```json
{
    "labelId": {标签},
    "comboName":{组合名称},
    "sceneDesc":{描述},
    "showOrder":{顺序}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.2.4 修改推荐组合

请求地址：/web/recommendDrugsCombo/saveDoc

请求参数：

```json
{
    "comboId":{ID},
    "labelId": {标签},
    "comboName":{组合名称},
    "sceneDesc":{描述},
    "showOrder":{顺序}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.2.5 新增推荐组合明细

请求地址：/web/recommendDrugsCombo/saveDtl

请求参数：

```json
{
    "comboId": {组合ID},
    "describe":{描述},
    "showOrder":{顺序},
    "recommendType":{推荐类别（1-主推，2-次推）}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.2.6 修改推荐组合明细

请求地址：/web/recommendDrugsCombo/saveDtl

请求参数：

```json
{
    "comboDtlId":{商品ID},
    "comboId": {组合ID},
    "describe":{描述},
    "showOrder":{顺序},
    "recommendType":{推荐类别（1-主推，2-次推）}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.2.7 删除推荐组合

请求地址：/web/recommendDrugsCombo/deleteDoc

请求参数：

```json
{
"ids":[组合ID]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.2.8 删除推荐组合明细

请求地址：/web/recommendDrugsCombo/deleteDtl

请求参数：

```json
{
"ids":[组合ID]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```



#### 2.3 价值标签

##### 2.3.1 查询

请求地址：/web/valueLabel/queryPage

请求参数：

```json
{
    "page":{
    "pageSize": {每页显示数量(必填)},
    "currentNum":{当前页(必填)}
                 },
    "valueLabelName":{价值标签ID/名称/助记码}
}
```

响应参数：

```json
{
    "code":0,
    "message":"操作成功",
    "data":{
        "content":[{
                "valueLabelId":{ID},
                "valueLabelName":{价值标签名称},
                "labelId":{标签ID},
            	"labelName":{标签名称},
                "valueLabelMCode":{助记码},
                "conditionList": [{
                    "screeningName":{条件名称},
                    "valueStart":{起始值},
                    "valueEnd":{终止值},
                    "dateStart":{起始时间},
                    "dateEnd":{终止时间}
                }],
                "dataCreated":{创建时间},
                "lastUpdated":{修改时间},
            	"status":{执行状态}//0未执行 1已执行 2执行中
            }]
      
    }
}
```

##### 2.3.2 保存

请求地址：/web/valueLabel/save

| 字段              | 字段中文名   | 可能的条件（start   <= value < end）                         |
| ----------------- | ------------ | ------------------------------------------------------------ |
| consumeCount      | 客单数       | dateStart ——dateEnd   valueStart——valueEnd                   |
| consumePrice      | 客单价       | dateStart ——dateEnd   valueStart——valueEnd                   |
| consumeAmount     | 累计消费     | dateStart ——dateEnd   valueStart——valueEnd                   |
| lastConsumeAmount | 末次消费金额 | valueStart——valueEnd                                         |
| issuanceDate      | 办卡日期     | dateStart ——dateEnd                                          |
| grossProfit       | 毛利额       | dateStart ——dateEnd   valueStart——valueEnd                   |
| grossProfitRate   | 毛利率       | dateStart ——dateEnd   valueStart——valueEnd                   |
| goods             | 购买过商品   | dateStart——dateEnd       ids （数据格式[商品ID]）（可以多选）4.2.1 |
| goodsSet          | 商品集合     | dateStart ——dateEnd  valueData（商品集合ID）（不可多选）     |

请求参数：

```json
{
   "valueLabelName":{价值标签名称},
   "valueLabelMCode":{价值标签助记码},
    "screeningList":[{
        "screeningName":{条件名},//(上表中的字段）
        "dateStart":{时间开始},
        "dateEnd":{时间结束},
        "valueStart":{值开始},
        "valueEnd":{值结束}
   }]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":{}
}
```



##### 2.3.3 删除

请求地址：/web/valueLabel/delete

请求参数：

```json
{
"ids":[ID]//valueLabelId 价值标签ID （如果对应标签绑定了会员 则返回不可删除）
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.3.4 查看明细

请求地址：/web/valueLabel/queryDtl

请求参数：

```
{
    "page":{
    "pageSize": {每页显示数量(必填)},
    "currentNum":{当前页(必填)}
                 },
    "valueLabelId":{价值标签ID}
}
```

响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "relationId": {会员明细Id},
                “labelId”:{标签ID},
                "valueLabelId": {价值标签Id },
                "valueLabelName": {标签名称 },
                "dataCreated":{明细创建时间},
                "lastUpdated": {明细修改时间},
                "memberId": {会员ID}
                "memberName": {姓名},
                "mobile": {电话},
                "issuanceDate": {开卡日期},
                "memberNo": {会员卡号},
                "curPoint": {当前积分},
                "sumConsumeAmount": {累计消费},
                "sex": {性别}, //1:男 2 女
                "nationName": {民族},
                "bloodType": {血型},
                "birthDate": {出生日期},
            }
        ]
    }
}
```



#### 2.4 标签属性管理

##### 2.4.1 保存

请求地址：/web/labelAttribute/save

请求参数：

```json
{
	"attributeId":{ID},
    "attributeName":{名称},
    "attributeMCode":{助记码}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.4.2 分页查询

请求地址：/web/labelAttribute/queryPage

请求参数：

```json
{
    "page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
    "attributeName":{ID/名称/助记码}
}
```

响应参数：

```json
{ "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "attributeId": {属性ID},
                "lastUpdated": {修改时间},
                "attributeName": {属性名称},
                "dataCreated": {创建时间},
                "attributeMCode": {属性助记码}
            }
        ],
    }
```

##### 2.4.3 删除

请求地址：/web/labelAttribute/delete

请求参数：

```json
{
"ids":[{属性ID}]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 2.4.4 查询

请求地址：/web/labelAttribute/queryAll

请求参数：

```json
{
    "attributeName":{ID/名称/助记码}
}
```

响应参数：

```json
{ "code": 0,
    "message": "操作成功",
    "data": [
            {
                "attributeId": {属性ID},
                "lastUpdated": {修改时间},
                "attributeName": {属性名称},
                "dataCreated": {创建时间},
                "attributeMCode": {属性助记码}
            }
        ]
    }
```

### 三、会员接口

#### 3.1 会员查询

##### 3.1.1 会员类型查询

请求地址：/web/member/queryMemberType

请求参数：

```json
{
    "memberTypeName":{标签iD/名称/助记码}
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
            {
                "memberTypeId": {会员类型ID},
                "memberTypeName": {会员类型名称},
                "memberTypeMCode": {助记码}
            }
        ]
    }
```



##### 3.1.2 会员分页查询

请求地址：/web/member/queryPage

请求参数：

```json
{
    "page":{
    "pageSize": {每页显示数量(必填)},
    "currentNum":{当前页(必填)}
                 },
    "memberName":{iD/名称/助记码},
    "memberTypeId":{会员类型Id},
    "mobile":{手机号},
    "memberNo":{会员卡号},
    "issuanceDateStart"：{开卡日期start},
    "issuanceDateEnd" : {开卡日期end},
	"userId":{客服ID}, // 3.2.8 查询客服
        
    "bindingStatus":{绑定状态}, //20211029 
    "bindingDateStart":{绑定时间从}, //20211029 
    "bindingDateEnd":{到}, //20211029 
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "curPoint":{当前积分},
                "nationName":{民族},
                "sumConsumeAmount": {累计消费},
                "sex": {性别}, //1:男 2 女
                "mobile": {"13847669677"},
                "memberName": {"徐翠微"},
                "bloodType": {血型},//0:A;1:B;2:O;3:AB.
                "birthDate": {出生日期 },//"1981-01-19 00:00:00"
                "useStatus": {会员状态}, // 0:停用 1：启用
                "memberNo": ｛会员卡号“XJ00704”｝,
                "lastUpdated": "2021-04-13 10:25:00",
                "issuanceDate": {开卡日期} ,//"2013-09-17 08:16:00"
                "dataCreated": "2013-05-13 11:32:00",
                "memberId": {会员Id}, //1624
                "memberTypeName":{会员类型},
                "customerService":{专属客服},
                "age":{年龄},
                "memberAge":{会龄},
                "lastConsumeDate":{末次消费日期},
                
                "qwMemberName":{企业微信名称}, //20211029 
                "qwMemberMemo":{企业微信备注}, //20211029 
                "bindingDate":{绑定日期}, //20211029 
                "bindingName":{绑定人}, //20211029 
                "bindingId":{绑定人Id}, //20211029
                "bindingStatus":{绑定状态} //20211029
            }
        ]
    }
```
##### 3.1.3 会员流水

请求地址：/web/member/queryRtlFlow

请求参数：

```json
{
    "page":{
    "pageSize": {每页显示数量(必填)},
    "currentNum":{当前页(必填)}
                 },
    "memberId":{会员iD}

}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "amount": {金额},
                "dataCreated": {日期},
                "goodsQty": ｛数量｝,
                "goodsName": {商品名称}
            }
        ]
    }
```

##### 3.1.4 会员消费占比

请求地址：/web/member/queryRtlRatioByMinClassName

请求参数：

```json
{
    "memberId":{会员iD}
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "amount":{消费金额},//1094572.72
            "minClassName": {类名}//"其他"
        }
    ]
}
```

##### 3.1.5 会员消费统计

请求地址：/web/member/queryRTLStatistics

请求参数：

```json
{
    "memberId":{会员iD}
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "amount": {消费金额},//"586.3"
        "price": {客单价},//"41.88"
        "count": {客单量}//"14"
    }
}
```

##### 3.1.6 批量设置专属客服

请求地址：/web/member/setCustomerService

请求参数：

```
{
    "memberIdList":[{会员ID}],
    "userId":{客服ID}// 3.2.8 查询客服 
}
```



响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 3.1.7 批量贴标签

请求地址：/web/member/setLabel

请求参数：

```json
{
    "memberIdList":[{会员ID}],
    "labelId":{客服ID},// 2.1.5 查询 goodsFlag 0 必填
    "adminId":{登录用户ID}
}
```

响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 3.1.8 查询近期新增的10条标签

请求地址：/web/label/queryTopLabel

请求参数：

``` 
{

}
```

响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "labelId":{标签ID},
            "labelName": {标签名称},
            "labelMCode": {标签助记码}
        }
    ]
}
  
```

##### 3.1.9 根据会员ID查询标签

请求地址：/web/member/queryLabelByMemberId

请求参数：

```
{
memberId:{会员ID}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "labelName": {标签名称},
            "labelColor": {标签颜色}
        }
    ]
}
```



### 3.2 会员群管理

##### 3.2.1 会员群分页查询

请求地址：/web/memberGroup/queryPage

请求参数：

```json
{
    "page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
    "memberSetName":{iD/名称/助记码},
    "dataCreatedStart":{创建时间从},
    "dataCreatedEnd":{到}
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "lastUpdated": {修改时间},
                "setWheres": {条件},
                "memo": {备注},
                "memberSetName": {会员群名称},
                "memberSetMCode": {助记码},
                "dtlCount": {会员数},
                "dataCreated": {创建时间},
                "memberSetId": {会员群ID},
                "state": "执行结束",
            }
        ]
    }
}
```



##### 3.2.2 会员群明细查询

请求地址：/web/memberGroup/queryMemberPageByGroupId

请求参数：

```json
{
    "page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
    "memberSetId":{会员群ID}
    
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "curPoint": {当前积分},
                "nationName": {民族},
                "sumConsumeAmount": {累计消费},
                "sex": {性别}, //1:男 2 女
                "mobile": {电话},
                "memberName": {姓名},
                "memberSetId": {会员群Id },
                "bloodType": {血型},
                "birthDate": {出生日期},
                "memberSetDtlId": {会员群明细Id},
                "lastUpdated": {修改时间},
                "memberNo": {会员卡号},
                "issuanceDate": {开卡日期},
                "dataCreated":{创建时间},
                "memberId": {会员ID}
            }
        ]
    }
}
```

##### 

##### 3.2.4 保存会员群 同时生成 群成员

请求地址：/web/memberGroup/save

请求参数：

```json
{
   "memberSetName":{会员群名称},
   "memberSetMCode":{助记码},
   "memo":{备注},
   "screeningList":[{
       "screeningName":{条件名},//(上表中的字段）
       "valueData":{等值},//(类似上表中的性别)
       "dateStart":{时间开始},
       "dateEnd":{时间结束},
       "valueStart":{值开始},
       "valueEnd":{值结束},
       "ids":[id]
   }]
    
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

###### 基础条件

| 字段                 | 字段中文名       | 可能的条件（start   <= value < end） |
| -------------------- | ---------------- | ------------------------------------ |
| vipflag              | VIP              | valueData 0:否 ；1：是               |
|                      |                  |                                      |
| lastPointConsumeDate | 末次积分消费日期 | dateStart ——dateEnd                  |
| sex                  | 性别             | 1：男 2：女                          |
| birthDate            | 生日             | dateStart ——dateEnd                  |
| issuanceDate         | 办卡时间         | dateStart ——dateEnd                  |
| curPoint             | 当前积分         | valueStart——valueEnd                 |
| useStatus            | 状态             | valueData 0：停用 1:正常             |

###### 消费条件

| 字段              | 字段中文名    | 可能的条件（start   <= value < end）       |
| ----------------- | ------------- | ------------------------------------------ |
| lastConsumeAmount | m末次消费金额 | valueStart——valueEnd                       |
| consumeCount      | 客单数        | dateStart ——dateEnd   valueStart——valueEnd |
| consumePrice      | 客单价        | dateStart ——dateEnd   valueStart——valueEnd |
| consumeAmount     | 累计消费      | dateStart ——dateEnd   valueStart——valueEnd |
| lastConsumeDate   | 末次消费日期  | dateStart ——dateEnd                        |
| grossProfitRate   | 毛利率        | dateStart ——dateEnd   valueStart——valueEnd |

###### 服务条件

##### 

| 字段            | 字段中文名 | 可能的条件（start   <= value < end） |
| --------------- | ---------- | ------------------------------------ |
| shopId          | 开卡门店   | valueData (门店ID)（不可以多选）     |
| customerService | 专属客服   | valueData  (userId)（不可以多选）    |
| oftenShop       | 常顾门店   | 稍等提供查询方法                     |
|                 |            |                                      |

###### 其他条件

##### 

| 字段         | 字段中文名 | 可能的条件（start   <= value < end）                         |
| ------------ | ---------- | ------------------------------------------------------------ |
| label        | 标签       | ids (数据格式[标签ID]) （可以多选）2.1.5 标签查询 goodsFlag:0 必填 |
| memberGroup  | 会员群     | valueData  3.2.1 会员群分页查询                              |
| goodsSet     | 商品集合   | dateStart——dateEnd       valueData (集合ID)（不可以多选）3.2.6 |
| goods        | 商品       | dateStart——dateEnd       ids （数据格式[商品ID]）（可以多选）4.2.1 |
| membertypeid | 会员类型   | 3.1.1 会员类型查询 ( 先修改为可多选 ) ids                    |

##### 3.2.5 查询门店

请求地址：/web/memberGroup/queryShop

请求参数：

```json
{
    "shopName":{Id/name/mcode}   
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "shopName": {门店名称},
            "shopId": {门店ID},
            "shopMCode": {门店助记码}
        }
    ]
}
```

##### 3.2.6 查询商品集合

请求地址：/web/memberGroup/queryGoodsSet

请求参数：

```json
{
    "goodsSetName":{Id/name/mcode}   
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "memo":{备注} ,
            "goodsSetMCode": {商品集合助记码},
            "goodsSetName": {商品集合名称},
            "goodsSetId": {商品集合ID}
        }
    ]
}：
```



##### 3.2.8 查询客服

请求地址：/web/memberGroup/queryUser

请求参数：

```json
{
    "userName":{Id/name/mcode}
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "userName": {姓名},
            "userId": {ID},
            "userMCode": {助记码}
        }
    ]
}
```

##### 3.2.9 会员群 查询

请求地址：/web/memberGroup/queryAll

请求参数：

```json
{
    "memberSetName":{iD/名称/助记码} 
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
                {
                    "memo": {备注},
                    "memberSetName": {会员群名称},
                    "memberSetMCode": {助记码},
                    "memberSetId": {会员群ID}
                }
        	]
    
}
```

#####  3.2.10 保存会员群 异步生成 群成员  和 （3.2.4 参数返回结果一致）

请求地址：/web/memberGroup/saven

请求参数：

```json
{
   "memberSetName":{会员群名称},
   "memberSetMCode":{助记码},
   "memo":{备注},
   "screeningList":[{
       "screeningName":{条件名},//(上表中的字段）
       "valueData":{等值},//(类似上表中的性别)
       "dateStart":{时间开始},
       "dateEnd":{时间结束},
       "valueStart":{值开始},
       "valueEnd":{值结束},
       "ids":[id]
   }]
    
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 3.2.11 删除会员群成员

请求地址：/web/member/delete

请求参数：

```json
{
    "memberSetDtlIdList":[
        memberSetDtlId的值(必填)long型
    ],
    "memberSetIdList":[
        memberSetId的值(必填)long型
    ]
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```



##### 3.2.12 新建高级群

请求地址：/web/member/creMemGroupList

请求参数：

```json
{
     "memberSetIdList":		[188,169], 【数值数组】
     "memberGroupInfo":              【会员群信息】
  {
    "memberSetName": "差集群",      
    "memberSetMCode": "MMMS",
    "memo":"创建",
    "setWheres":null
  },
  "ope":3     【 1 交集，2并集，3 差集】
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data":null
}
```

### 四、 商品接口

#### 4.1 商品管理

##### 4.1.1商品分页查询

请求地址：/web/goods/goodsQueryPage

请求参数：

```json
{
    "page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
    "goodsName":{Id/name/mcode},
    "goodsSpecs":{规格},
    "factoryName":{Id/name/mcode},
                     
    "goodsShortName":{商品简称},
    "financialNo":{"财务编码"},
    "goodsNo":{商品编号},
    "medCineFormId":{剂型},//4.1.1.1 查询剂型
    "approvalNo":{批准文号},
    "purTaxRateStart":{采购税率从},
    "purTaxRateEnd":{到},
    "saTaxRateStart":{销售税率从},
    "saTaxRateEnd":{到},
    "minClassId":{商品小类},//4.1.1.2 查询商品小类
    "midClassId":{商品中类},//4.1.1.3 查询商品中类
    "mainClassId":{商品大类},//4.1.1.4 查询商品大类
    "goodsBarCode":{条形码},
    "goodsSpecNo":｛商品特殊编码｝,
    "transConditionId":｛运输条件｝,//4.1.1.5 查询运输方式
    "keepConditionId":{储存条件},//4.1.1.6 查询存储方式
    "otcFlag":{OTC},//4.1.1.7 查询OTC
    "operateRangeId":{经营范围},//4.1.1.8 查询经营范围
    "otherClassId":{其他分类},//4.1.1.9 查询其他分类
    "grossProfitClassId":{毛利率分类},//4.1.1.10 查询毛利率分类
    "lotFlag":{管理批号}, //0否 1 是
    "descFlag":{电子监管}, //0否 1 是
    "doubleFlag":{双验双复},//0否 1 是
    "ephedrineFlag":{含麻药品},//0否 1 是
    "poisonousFlag":{毒性药品},//0否 1 是
    "psychotropic":{精神类药品},//0否 1 是
    "narcotics":{麻醉药品},//0否 1 是
    "vaccines":{疫苗},//0否 1 是
    "bioFlag":{生物制剂},//0否 1 是
    "cnPatentDrug":{中成药},//0否 1 是
    "importFlag":{进口药品},//0否 1 是
    "socialFlag":{社保药品},//0否 1 是
    "gmpFlag":{GMP标识},//0否 1 是
    "patentFlag":{专利药},//0否 1 是
    "importantFlag":{重点养护},//0否 1 是
    "chnCrude":{中药饮片},//0否 1 是
    "finishFlag":{回执管理标志},//0否 1 是
    "coldFlag":{冷链标志},//0否 1 是
    "idInformationFlag":{实名购买},//0否 1 是                
    "labelId":{标签ID}, 
                     
    "labelAttributeDTOList":[
        {
            "attributeId":{标签属性ID}//见 标签属性 2.4.4 查询
        }
         ]
    
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "approvalNo": {批准文号},
                "descFlag": {电子监管}, //0否 1 是
                "finishFlag": {回执管理标志}, //0否 1 是
                "mainClass":{商品大类},
                "goodsShortName": {商品简称},
                "midClass": {商品种类},
                "vaccines": {疫苗标识}, //0否 1 是
                "goodsId": {商品ID},
                "keepCondition": {储藏条件},
                "idInformationFlag": {实名购买}, //0否 1 是
                "factoryMCode": {厂家助记码},
                "chnCrude": {中药饮片}, //0否 1 是
                "goodsBarCode": {条形码},
                "importantFlag": {重点养护}, //0否 1 是
                "psychotropic": {精神类药品}, //0否 1 是
                "lotFlag": {管理批号}, //0否 1 是
                "patentFlag": {专利药}, //0否 1 是
                "poisonousFlag": {毒性药品}, //0否 1 是
                "transCondition": {运输条件},
                "ephedrineFlag": {含麻药品}, //0否 1 是
                "importFlag": {进口药品}, //0否 1 是
                "bioFlag": {生物制剂}, //0否 1 是
                "cnPatentDrug": {中成药}, //0否 1 是
                "goodsName": {商品名称},
                "otherClass": {其他分类},
                "saTaxRate": {销售税率},
                "purTaxRate": {采购税率},
                "goodsMCode": {商品助记码},
                "financialNo": {财务编码},
                "goodsNo": {商品编码},
                "minClass": {商品小类},
                "coldFlag": {冷链标识}, //0否 1 是
                "factoryName": {厂家名称},
                "operateRange": {经营范围},
                "goodsSpecs": {规格},
                "otcFlag": {OTC},
                "labelList": [
                    {
                            "labelId":{标签ID},
                            "labelName": {标签名称},
                            "labelMCode": {标签助记码},
                            "describe": {描述},
                            "taboo": {禁忌},
                            "labelColor": {标签颜色},//"#2ad643"
                            "dataCreated": {创建时间},
                            "lastUpdated": {修改时间}
                    }
                ],
                "narcotics": {麻醉药品}, //0否 1 是
                "goodsSpecNo": {商品特殊编码},
                "medCineForm": {剂型},
                "doubleFlag":{双验双复}, //0否 1 是
                "gmpFlag": {GMP标识}, //0否 1 是
                "grossProfitClass": {毛利分类},
                "socialFlag": {社保药品},  //0否 1 是
                "useStatus":{商品状态}// 0停用 1正常 2业务停用
            	}
            
   	 		]
	}
}
```

###### 4.1.1.1 查询剂型

请求地址：/web/goods/queryMedCineForm

请求参数：

```json
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "medCineFormId": {剂型ID},
            "medCineForm": {剂型}
        }
    ]
}
```

  

###### 4.1.1.2 查询商品小类

请求地址：/web/goods/queryMinClass

请求参数：

```
{
"minClassName":{ID/名称},
"midClassId"{中类ID}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "minClassId": {小类ID},
            "minClassName": {小类名称},
            "minClassMCode": {小类助记码}
        }
        ]
}
```

  

###### 4.1.1.3 查询商品中类

请求地址：/web/goods/queryMidClass

请求参数：

```
{
"midClassName":{ID/名称},
"mainClassId"{大类ID}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "midClassMCode": {助记码},
            "midClassName": {中类名称},
            "midClassId":{中类ID}
        }
        ]
 }
```

  

###### 4.1.1.4 查询商品大类

请求地址：/web/goods/queryMainClass

请求参数：

```
{
"mainClassName":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "mainClassId": {ID},
            "mainClassName": {名称},
            "mainClassMCode": {助记码｝
        }
        ]
  }
        
```

  

###### 4.1.1.5 查询运输方式

请求地址：/web/goods/queryTransCondition

请求参数：

```
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "transConditionId": {ID},
            "transCondition": {名称}
        }
    ]
}
```

  

###### 4.1.1.6 查询储存条件

请求地址：/web/goods/queryKeepCondition

请求参数：

```
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "keepCondition": {名称},
            "keepConditionId": {ID}
        }
    ]
}
```

  

###### 4.1.1.7 查询OTC

请求地址：/web/goods/queryOTC

请求参数：

```
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "keyName": {名称},
            "keyId": {ID}
        }
    ]
}
```

  

###### 4.1.1.8 查询经营范围

请求地址：/web/goods/queryOperateRange

请求参数：

```
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "operateRange": {名称},
            "operateRangeId": {ID}
        }
    ]
}
```

  

###### 4.1.1.9 查询其他分类

请求地址：/web/goods/queryOtherClass

请求参数：

```
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "otherClassName": {名称},
            "otherClassId": {ID}
        }
    ]
}
```

  

###### 4.1.1.10 查询毛利率分类

请求地址：/web/goods/queryGrossProfitClass

请求参数：

```
{
	"name":{ID/名称}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "rate": {毛利率},
            "grossProfitClass": {名称},
            "grossProfitClassId": {ID}
        }
    ]
}
```

  



##### 4.1.2 查询商品 

请求地址：/web/goods/goodsQueryAll

请求参数：

```json
{
    "goodsName":{Id/name/mcode},
    "goodsSpecs":{规格},
    "factoryName":{Id/name/mcode},
    "labelAttributeDTOList":[
        {
            "attributeId":{标签属性ID}//见 标签属性 2.4.4 查询
        }
         ]
    
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
            	{
                       "approvalNo": {批准文号},
                "descFlag": {电子监管},
                "finishFlag": {回执管理标志},
                "mainClass":{商品大类},
                "goodsShortName": {商品简称},
                "midClass": {商品种类},
                "vaccines": {疫苗标识},
                "goodsId": {商品ID},
                "keepCondition": {储藏条件},
                "idInformationFlag": {实名购买},
                "factoryMCode": {厂家助记码},
                "chnCrude": {中药饮片},
                "goodsBarCode": {条形码},
                "importantFlag": {重点养护},
                "psychotropic": {精神类药品},
                "lotFlag": {管理批号},
                "patentFlag": {专利药},
                "poisonousFlag": {毒性药品},
                "transCondition": {运输条件},
                "ephedrineFlag": {含麻药品},
                "importFlag": {进口药品},
                "bioFlag": {生物制剂},
                "cnPatentDrug": {中成药},
                "goodsName": {商品名称},
                "otherClass": {其他分类},
                "saTaxRate": {销售税率},
                "purTaxRate": {采购税率},
                "goodsMCode": {商品助记码},
                "financialNo": {财务编码},
                "goodsNo": {商品编码},
                "minClass": {商品小类},
                "coldFlag": {冷链标识},
                "factoryName": {厂家名称},
                "operateRange": {经营范围},
                "goodsSpecs": {规格},
                "otcFlag": {OTC},
                "labelList": [
                    {
                            "labelId":{标签ID},
                            "labelName": {标签名称},
                            "labelMCode": {标签助记码},
                            "describe": {描述},
                            "taboo": {禁忌},
                            "labelColor": {标签颜色},//"#2ad643"
                            "dataCreated": {创建时间},
                            "lastUpdated": {修改时间}
                    }
                ],
                "narcotics": {麻醉药品},
                "goodsSpecNo": {商品特殊编码},
                "medCineForm": {剂型},
                "doubleFlag":{双验双复},
                "gmpFlag": {GMP标识},
                "grossProfitClass": {毛利分类},
                "socialFlag": {社保药品} 
            	}
   	 		]
}
```



##### 4.1.3 贴标签

请求地址：/web/goods/setLabel

请求参数：

```json
{
	"goodsId":{商品ID}, 
    "labelIdList":[{标签ID}] //2.1.5 查询 goodsFlag:1 必填
    
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 4.1.4  根据商品Id 查询 用药提醒

请求地址：/web/remindGoods/queryStrategyByGoodsId

请求参数：

```
{
	"page":{
        "pageSize": 10,
        "currentNum":1
                 },
  	"goodsId" : {商品Id}
}
```

响应参数

```
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "strategyName": {策略名称},
                "lastUpdated": {修改时间},
                "strategyMemo": {备注},
                "goodsId": {商品Id},
                "factoryName": {生产厂家},
                "strategyId": {策略ID},
                "dataCreated": {创建时间},
                "goodsName": {商品名称},
                "goodsSpecs": {规格}
                
                "relationId": {关系ID},
                
                "wxFlag": {微信标识},
                "automatic": {消费标识},
                "mouldId": {微信模板ID},
                "msgName": {微信模板},
                "sendAfterOfSell": {延迟天数},
                "msmFlag": {短信内容},
                "remindTo": {提醒对象},
                "wxMsgContent": {微信内容},
                "messageContent": {短信内容}
            }
            }
        ]
    }
}
```

##### 4.1.5 执行策略查询

请求地址：/web/remindGoods/getStrategy

请求参数：

```
{
"strategyId":{执行策略ID}
}
```

相应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "strategyId": {策略ID},
        "strategyName": {策略名称},
        "memo": {备注},
        "sendAfterOfSell": {延迟天数},
        "messageContent": {短信内容},
        "mouldId": {微信模板ID},
        "wxMsgContent": {微信内容},
        "automatic": {消费标识},
        "wxFlag": {微信标识},
        "msmFlag": {短信标识},
        "remindTo": {推送对象},
        "dataCreated": {创建时间},
        "lastUpdated": {修改时间}
    }
}
```



##### 4.1.6 执行策略保存

请求地址：/web/remindGoods/save

请求参数：

```json
{
	"strategyId":{ID},
    "strategyName":{策略名称},
    "memo":{备注},
    "sendAfterOfSell":{零售后n天提醒},
    "automatic":{消费标识},//消费标识选中 则不可以填写延迟天数 
    "wxFlag":{微信标识},//0：不发送 1 发送,
    "msmFlag":{短信标识},//0：不发送 1 发送,
    "goodsId":{商品ID},
    "remindTo":{提醒对象（1：店员 2：会员）},
    "messageContent":{消息内容｝,//【内容中可以添加字段 目前只支持一下两个 会员姓名 @memberName@  商品名称 @goodsName@  例如 尊敬的会员 @memberName@ ：您购买的 @goodsName@ 请按时服用，祝您早日康复！】                
    "mouldId":{微信模板Id},// 4.2.5  微信模板 查询
    "wxMsgContent":{微信消息内容}
    	//	[{
        // 		paramName:{参数名称}, // 4.2.5  微信模板 查询
        //		paramValue:{参数值 可包含 会员姓名 @memberName@  商品名称 @goodsName@ }
        //	}]
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 4.1.7  用药提醒 删除

请求地址：/web/remindGoods/delete

请求参数：

```json
{
	"ids":[id]// relationId 关系ID
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```



##### 4.1.8  微信模板 查询

请求地址：/web/goods/queryAllWxMould

请求参数：

```json
{	
	"msgName":{ID/名称}
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "lastUpdated": {修改时间},
            "mouldId": {模板ID},
            "forExample": {实例},
            "msgName": {模板名称},
            "paramList": [
                   {
                    "cnName": {参数中文名},
                    "paramName": {参数名},
                    "paramValue": {参数值}
                }
            ],
            "dataCreated": {创建时间}
        }
    ]
}
```



### 五 营销管理

#### 5.1 新会员策略	

[（需要消费标识 spendFlag 执行条件）]()

##### 5.1.1 新会员策略查询

请求地址：/web/memberStrategy/newMemberStrategyQueryPage

请求参数：

```json
{	"page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
	"strategyName":{ID/名称},
    "remindType":{提醒方式（1：短信；2：微信,3:微信+短信）},
    "remindTo":{提醒对象（1：店员 2：会员）}
    
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "strategyName": {策略名称},
                "memo": {备注},
                "lastUpdated": {修改时间},
                "mouldId": {微信模板ID},
                "msgName": {微信模板},
                "sendAfterOfSell":{延迟天数},
                "automatic":{新会员触发},
    			"wxFlag":{微信标识},
    			"msmFlag":{短信标识},
                
                "remindTo": {提醒对象 1、店员 2、会员},
//  不展示              "newFlag": {新会员标识 0、否 1、是},
//  不展示              "memberGroupId": {会员群ID},
//  不展示              "memberGroupName":{会员群},
                "strategyId": {策略ID},
                "dataCreated": {创建时间},
                "wxMsgContent": {微信内容},
                "messageContent": {短信内容}
            }
        ]
    }
}
```

##### 5.1.2 新会员策略保存

请求地址：/web/memberStrategy/saveNewMemberStrategy

请求参数：

```json
{
   "strategyId":{策略ID},
   "strategyName":{策略名称},
    "memo":{备注},
    "sendAfterOfSell":{延迟发送天数},
    "messageContent":{短信内容},
    "mouldId":{微信模板ID},
  
    "automatic":{新会员触发}, //0: ；1：新会员触发 值为0时可输入延迟天数；
    "wxFlag":{微信标识},
    "msmFlag":{短信标识},
    
    "remindTo":{提醒对象1：店员 2：会员},
    "wxMsgContent":{微信内容}//[{"paramName":{参数名},"paramValue":{参数值}}]
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 5.1.3 删除

请求地址：/web/memberStrategy/delete

请求参数：

```json
{
"ids":[ID]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

#### 5.2 会员策略

[（需要 会员群 做执行条件）]()

##### 5.2.1 会员策略查询

请求地址：/web/memberStrategy/memberStrategyQueryPage

请求参数：

```json
{	"page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
	"strategyName":{ID/名称},
    "remindType":{提醒方式（1：短信；2：微信,3:微信+短信）},
    "remindTo":{提醒对象（1：店员 2：会员）}
    
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
            	"strategyName": {策略名称},
                "memo": {备注},
                "lastUpdated": {修改时间},
                "mouldId": {微信模板ID},
                "msgName": {微信模板},
                "remindTo": {提醒对象 1、店员 2、会员},
                "memberGroupId": {会员群ID},
                "memberGroupName":{会员群},
                "strategyId": {策略ID},
                "dataCreated": {创建时间},
                "wxMsgContent": {微信内容},
                "automatic":{消费标识},//0: ；1：消费时触发 
    			"wxFlag":{微信标识},// 1：发送微信
    			"msmFlag":{短信标识},//2：发送短信
                "messageContent": {短信内容},
                "allMemberFlag": {全部会员}
            }
            }
        ]
    }
}
```

##### 5.2.2 会员策略保存

请求地址：/web/memberStrategy/saveMemberStrategy

请求参数：

```json
{
   "strategyId":{策略ID},
   "strategyName":{策略名称},
    "memo":{备注},
    "messageContent":{短信内容},
    "mouldId":{微信模板ID},
    "automatic":{消费标识},//0: ；1：消费时触发（为0时可手动发送）5.2.4 执行发送会员的略信息
    "wxFlag":{微信标识},// 1：发送微信
    "msmFlag":{短信标识},//2：发送短信
    "remindTo":{提醒对象1：店员 2：会员},
    "memberGroupId":{会员群ID},
   "wxMsgContent":{微信内容}, //[{"paramName":{参数名},"paramValue":{参数值}}]
    "allMemberFlag":{全部会员} //0 否 1 是
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 5.2.3 删除（与5.1.3 相同）

请求地址：/web/memberStrategy/delete

请求参数：

```json
{
"ids":[ID]
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 5.2.4 执行发送会员策略信息

请求地址：/web/memberStrategy/activeSendMsg

请求参数：

```json
{
"strategyId":{ID}
}
```

响应参数：

```json
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```



#### 

#### 5.3 回访任务

##### 5.3.1 分页查询

请求地址：/web/returnVisit/queryPage

请求参数：

```json
{	"page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
	"name":{ID/名称},
    "shopId":{指派门店}
}
```

响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "lastUpdated": {修改时间},
                "visitScene": {回访场景},
                "name": {任务名称},
                "memberGroupId":{会员群},
                "memberGroupName": {会员群名称},
                "visitContent": {回访内容},
                "id": {任务ID},
                "shopId": {指派门店},
                "shopName": {门店名称},
                "dataCreated": {创建时间}，
                "startDate":{开始时间}，
                "endDate":{结束时间}
            }
        ]
        }
 }
```

##### 5.3.2 查询

请求地址：/web/returnVisit/save

请求参数：

```
{
	"name":{ID/名称}，
	"shopId":{指派门店}
}
```

响应参数：

```
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "lastUpdated": {修改时间},
            "visitScene": {回访场景},
            "name": {任务名称},
            "memberGroupId":{会员群},
            "memberGroupName": {会员群名称},
            "visitContent": {回访内容},
            "id": {任务ID},
            "shopId": {指派门店},
             "shopName": {门店名称},
            "dataCreated": {创建时间}
        }
    ]
}
```

##### 5.3.3 保存

请求地址：/web/returnVisit/save

请求参数：

```
{
	 "id": {任务ID},
	 "name": {任务名称},
	 "memberGroupId":{会员群},
     "visitScene": {回访场景},
     "visitContent": {回访内容},
     "shopId": {指派门店},
     "dataCreated": {创建时间},
     "lastUpdated": {修改时间}，
     "startDate":{开始时间}，
     "endDate":{结束时间}
     
}
```

响应参数：

```
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

##### 5.3.4 删除

请求地址：/web/returnVisit/delete

请求参数：

```
{
 "ids":[{ID}]    
}
```

响应参数：

```
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

### 六、店员管理

#### 6.1 店员查询

请求地址：/web/user/queryUser

请求参数：

```
{
 "userName":{ID/name/手机/门店ID}，
 "headFlag":{店长标识} //0 否 1 是
 "page":{
        "pageSize": {每页显示数量(必填)},
        "currentNum":{当前页(必填)}
                 },
}
```



响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "grapeUserName": {grape用户名},
                "shopShortName": {门店简称},
                "mobile": {手机号},
                "qyUserId":{企业微信用户},
                "shopName":{门店名称},
                "shopId":{门店ID},
                "userName":{用户名},
                "userId":{用户ID},
                "grapeUserId":{grape用户Id},
                "headFlag":{店长标识} //0 否 1 是
            }
        ]
    }

}
```



#### 6.2 批量设置店长

请求地址：/web/user/setShopHead

请求参数：

```
{
"ids":[userId]
}
```



响应参数：

```
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```



#### 6.3批量取消店长

请求地址：/web/user/cancelShopHead

请求参数：

```
{
"ids":[userId]
}
```



响应参数：

```
{
    "code":0, 
    "message":"操作成功",
	"data":null
}
```

### 七、复购

##### 7.1 保存复购消息

请求地址：/web/goodsrepurchese/addRepurchaseDate

请求参数：

```json
{
	"rtlDtlId": 9909751,                          【细单ID，必填 ，为空时，可新建提醒消息】
	"memberId": 1607,                               【会员ID，必填】
	"repurchaseSetDate": "2021-07-02 12:00:00",              【设置复购时间，必填】
	"repurchaseMemo":"测试备注",                     【备注】
						【其他字段还用药提醒设置一致】
	"strategyId":{ID},    
    "memo":{备注},
    "wxFlag":{微信标识},//0：不发送 1 发送,
    "msmFlag":{短信标识},//0：不发送 1 发送,
    "goodsId":{商品ID},                                    【必填】
    "remindTo":{提醒对象（2：会员）},
    "messageContent":{消息内容｝,//【内容中可以添加字段 目前只支持一下两个 会员姓名 @memberName@  商品名称 @goodsName@  例如 尊敬的会员 @memberName@ ：您购买的 @goodsName@ 请按时服用，祝您早日康复！】                
    "mouldId":{微信模板Id},// 4.2.5  微信模板 查询
    "wxMsgContent":{微信消息内容}
    	//	[{
        // 		paramName:{参数名称}, // 4.2.5  微信模板 查询
        //		paramValue:{参数值 可包含 会员姓名 @memberName@  商品名称 @goodsName@ }
        //	}]
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": null
}
```

##### 7.2查询复购消息

请求地址：/web/goodsrepurchese/queryStrategyById

请求参数：

```json

{
	"page":{
        "pageSize": 10,
        "currentNum":1
                 },
  	"trlDtlId" : 9909751
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": [
        {
            "wxFlag": 1,
            "strategyName": "购买提醒",
            "goodsId": 21260,
            "repurchaseMemo": "测试",            【复购备注】
            "factoryName": "云南白药集团大理药业有限责任公司",
            "automatic": 1,
            "goodsSpecs": "6g*10袋",
            "lastUpdated": "2021-06-28 18:45:28",
            "strategyMemo": null,
            "mouldId": 3,
            "sendAfterOfSell": 0,
            "rtlDtlId": 9909751,
            "msmFlag": 1,
            "remindTo": 2,
            "repurchaseSetDate": "2021-07-01 00:00:00",               【复购提醒时间】
            "msgName": "用药时间",
            "strategyId": 29,
            "dataCreated": "2021-06-28 18:45:28",
            "wxMsgContent": "[{\"paramName\":\"first\",\"paramValue\":\"11\"},{\"paramName\":\"keyword1\",\"paramValue\":\"22\"},{\"paramName\":\"keyword2\",\"paramValue\":\"33\"},{\"paramName\":\"remark\",\"paramValue\":\"44\"}]",
            "goodsName": "六味地黄丸",
            "repurchaseId": 1,
            "messageContent": "你好，您购买了镇痛类药物，请按照说明进行服用或遵医嘱"
        }
    ]
}
```


##### 7.3商品复购查询

请求地址：/wm_crm/web/goodsrepurchese/query

请求参数：

```json
{
"startDate":"2021-06-01 15:04:40",    【最好两个时间让他都选择，不选择就是默认三十天前的数据】
    "endDate":"2021-06-30 15:04:40",   
    "memberMobile":"132111",     【会员手机号】
    "memberName":"王玉花",       【会员名称】
    "goodsName":"六味地黄丸",     【商品名称】
    "shopName":"喀喇沁旗荣济堂医药有",   【门店名称】
     "page":		
	  {
	    "currentNum": 1,
	    "pageSize": 20
	  }
}
```

响应参数：

```json
{
    "code": 0,
    "message": "操作成功",
    "data": {
        "content": [
            {
                "medcineform": 6,                                【剂型】
                "goodsId": 21260, 
                "buyDate": "2021-06-05 09:23:33",            【购买时间】
                "repurchaseMemo": null,                                 【复购备注】
                "memberName": "王玉花",
                "shopName": "喀喇沁旗荣济堂医药有限责任公司卫生所",
                "factoryName": "云南白药集团大理药业有限责任公司",
                "goodsQty": 1,                                               【商品数量】
                "goodsSpecs": "6g*10袋",		【规格】
                "memberNo": "XJ00687",                                  【会员卡号】
                "rtlDtlId": 9909751,
                "repurchaseSetDate": null,                              【复购提醒时间】
                "shopId": 245,             
                "goodsName": "六味地黄丸",
                "repurchaseId": null,                              【商品复购对应ID】
                "memberMobile": "13211111111",
                "rtlDocId": 4737375,
                "memberId": 1607
            }
        ],
        "pageable": {
            "sort": {
                "sorted": false,
                "unsorted": true,
                "empty": true
            },
            "offset": 0,
            "pageNumber": 0,
            "pageSize": 20,
            "paged": true,
            "unpaged": false
        },
        "totalElements": 1,
        "totalPages": 1,
        "last": true,
        "number": 0,
        "size": 20,
        "sort": {
            "sorted": false,
            "unsorted": true,
            "empty": true
        },
        "numberOfElements": 1,
        "first": true,
        "empty": false
    }
}
```